var requestVerb = context.getVariable("request.verb");

if((requestVerb === "PUT") || (requestVerb === "POST"))
{
    try{
        
        var requestContent = context.getVariable("message.content");
        JSON.parse(requestContent);
    }
    catch(err)
    {
        context.setVariable("errorJSON","invalid_json_format");
        throw "InvalidJsonError";
    }
    
}